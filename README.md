# Stock Exchange CSS Tables & Forms Practice #

Using CSS to add styling to tables and forms.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open your local copy folder with Sublime, there is a HTML document, a JS file and CSS style sheets.
2. Add your style rules to style.css, this file is linked to from stock-exchange.html.
3. Open stock-exchange.html by typing in terminal 'open stock-exchange.html', this is a page with stock market stats and a search form.
4. Add style rules that make the table data easier to use and look at, add your rules to style.css.
5. Add style rules that make the search form easier to use and look at, add your rules to style.css.
6. You can see your progress by typing in terminal 'open stock-exchange.html'.

(The declarations you wrote in style.css took precedence over the ones found in starter.css because of the order they are loaded in.)
